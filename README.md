# Tournament

Загрузка базы:
- bin/console doctrine:database:create --env=dev --no-interaction
- bin/console doctrine:migrations:migrate --env=dev --no-interaction
- bin/console doctrine:database:create --env=test --no-interaction
- bin/console doctrine:fixtures:load --env=test --no-interaction

Проверка кода:
- vendor/bin/php-cs-fixer fix --diff --dry-run -v
- vendor/bin/phpstan analyze src --memory-limit 256M --no-progress -c phpstan.neon

Тесты:
- vendor/bin/codecept run functional
- vendor/bin/codecept run unit