<?php

namespace App\Controller;

use App\Utils\Builder\BracketBuilder;
use App\Utils\Builder\MatrixBuilder;
use App\Utils\Generator\DataGenerator;
use App\Utils\Matches\MatchesService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class TournamentController extends AbstractController
{
    public function index(
        DataGenerator $dataGenerator,
        MatrixBuilder $matrixBuilder,
        MatchesService $matchesService
    ): Response {
        $teams['team1'] = $dataGenerator->getTeams();
        $divisions['division1'] = $matrixBuilder->build($teams['team1']);

        $teams['team2'] = $dataGenerator->getTeams();
        $divisions['division2'] = $matrixBuilder->build($teams['team2']);

        $matchesService->setTotal($divisions);

        return $this->render('tournament/index.html.twig', [
            'divisions' => $divisions,
            'teams' => $teams,
        ]);
    }

    public function button(BracketBuilder $bracketBuilder, MatchesService $matchesService): Response
    {
        $result = $bracketBuilder->build();

        return new JsonResponse($result);
    }
}
