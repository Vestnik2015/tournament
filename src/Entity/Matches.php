<?php

namespace App\Entity;

use App\Repository\MatchesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MatchesRepository::class)
 */
class Matches
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="m_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $id;

    /**
     * @ORM\Column(name="m_team", type="string", length=128, nullable=false)
     */
    private $team;

    /**
     * @ORM\Column(name="m_total", type="integer", nullable=false)
     */
    private $total;

    /**
     * @ORM\Column(name="m_division", type="integer", nullable=false)
     */
    private $division;

    /**
     * @ORM\Column(name="m_stage", type="string", length=128, nullable=false)
     */
    private $stage;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTeam(): string
    {
        return $this->team;
    }

    public function setTeam(string $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getDivision(): int
    {
        return $this->division;
    }

    public function setDivision(int $division): self
    {
        $this->division = $division;

        return $this;
    }

    public function getStage(): string
    {
        return $this->stage;
    }

    public function setStage(string $stage): self
    {
        $this->stage = $stage;

        return $this;
    }
}
