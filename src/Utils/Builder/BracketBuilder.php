<?php

namespace App\Utils\Builder;

use App\Repository\MatchesRepository;
use App\Utils\Generator\DataGenerator;

class BracketBuilder
{
    private DataGenerator $dataGenerator;

    private MatchesRepository $matchesRepository;

    public function __construct(
        DataGenerator $dataGenerator,
        MatchesRepository $matchesRepository
    ) {
        $this->dataGenerator = $dataGenerator;
        $this->matchesRepository = $matchesRepository;
    }

    public function build(): array
    {
        $quarterFinal1 = $this->matchesRepository->findBy(
            [
                'division' => 1,
                'stage' => 'division',
            ],
            ['total' => 'DESC'],
            4
        );

        $quarterFinal2 = $this->matchesRepository->findBy(
            [
                'division' => 2,
                'stage' => 'division',
            ],
            ['total' => 'DESC'],
            4
        );

        $result = [
            'teams' => [
                [$quarterFinal1[0]->getTeam(), $quarterFinal1[1]->getTeam()],
                [$quarterFinal1[2]->getTeam(), $quarterFinal1[3]->getTeam()],
                [$quarterFinal2[0]->getTeam(), $quarterFinal2[1]->getTeam()],
                [$quarterFinal2[2]->getTeam(), $quarterFinal2[3]->getTeam()],
            ],
            'results' => [
                $this->dataGenerator->getQuarterFinalResult(),
                $this->dataGenerator->getSemiFinalResult(),
                $this->dataGenerator->getFinalResult(),
            ],
        ];

        return $result;
    }
}
