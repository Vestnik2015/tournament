<?php

namespace App\Utils\Builder;

use App\Utils\Comparator\TeamsComparator;
use App\Utils\Generator\DataGenerator;

class MatrixBuilder
{
    private DataGenerator $dataGenerator;

    private TeamsComparator $teamsComparator;

    public function __construct(
        DataGenerator $dataGenerator,
        TeamsComparator $teamsComparator
    ) {
        $this->dataGenerator = $dataGenerator;
        $this->teamsComparator = $teamsComparator;
    }

    public function build(array $teams): array
    {
        $data = $this->dataGenerator->getData($teams);

        $matrix = [];
        $score = [];
        for ($i = 0; $i < DataGenerator::COUNT_TEAMS; $i++) {
            for ($j = 0; $j < DataGenerator::COUNT_TEAMS; $j++) {
                if ($i === $j) {
                    $matrix[$i][$j] = '';
                    continue;
                }

                if (!isset($score[$i])) {
                    $score[$i]['total'] = 0;
                }

                $match = $this->teamsComparator->compare($data, $teams[$i], $teams[$j]);

                if ($match) {
                    $matrix[$i][$j]['result'] = $match[0]['result1'] . ':' . $match[0]['result2'];
                    $score[$i]['total'] += $match[0]['result1'];
                    $score[$i]['team'] = $teams[$i];
                    continue;
                }

                $matchReverse = $this->teamsComparator->compareReverse($data, $teams[$i], $teams[$j]);

                if ($matchReverse) {
                    $matrix[$i][$j]['result'] = $matchReverse[0]['result2'] . ':' . $matchReverse[0]['result1'];
                    $score[$i]['total'] += $matchReverse[0]['result2'];
                    $score[$i]['team'] = $teams[$i];
                }
            }
        }

        $result['matrix'] = $matrix;
        $result['score'] = $score;

        return $result;
    }
}
