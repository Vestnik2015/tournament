<?php

namespace App\Utils\Comparator;

class TeamsComparator
{
    public function compare(array $data, string $team1, string $team2): array
    {
        return array_values(array_filter($data, function ($v) use ($team1, $team2) {
            return $v['team1'] == $team1 && $v['team2'] == $team2;
        }));
    }

    public function compareReverse(array $data, string $team1, string $team2): array
    {
        return array_values(array_filter($data, function ($v) use ($team1, $team2) {
            return $v['team2'] == $team1 && $v['team1'] == $team2;
        }));
    }

    public function compareTeamScore(int $team1score, int $team2score): bool
    {
        return ($team1score > $team2score);
    }
}
