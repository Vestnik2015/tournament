<?php

namespace App\Utils\Generator;

use App\Utils\Comparator\TeamsComparator;
use Faker\Factory;
use Faker\Generator;

class DataGenerator
{
    const COUNT_TEAMS = 8;

    private Generator $faker;

    private TeamsComparator $teamsComparator;

    public function __construct(
        TeamsComparator $teamsComparator
    ) {
        $this->faker = Factory::create();
        $this->teamsComparator = $teamsComparator;
    }

    public function getTeams(): array
    {
        $teams = [];
        for ($i = 0; $i < self::COUNT_TEAMS; $i++) {
            $teams[] = $this->faker->word() . $this->faker->numberBetween(0, 100);
        }

        return $teams;
    }

    public function getData(array $teams): array
    {
        $data = [];
        for ($i = 0; $i < self::COUNT_TEAMS; $i++) {
            for ($j = 0; $j < self::COUNT_TEAMS; $j++) {
                $team = $this->teamsComparator->compare($data, $teams[$i], $teams[$j]);
                $teamReverse = $this->teamsComparator->compareReverse($data, $teams[$i], $teams[$j]);

                if ($team || $teamReverse) {
                    continue;
                }

                if ($teams[$i] != $teams[$j]) {
                    $data[] = [
                        'team1' => $teams[$i],
                        'team2' => $teams[$j],
                        'result1' => $this->faker->numberBetween(0, 7),
                        'result2' => $this->faker->numberBetween(0, 7),
                    ];
                }
            }
        }

        return $data;
    }

    public function getQuarterFinalResult(): array
    {
        return $this->getResultGames(4);
    }

    public function getSemiFinalResult(): array
    {
        return $this->getResultGames(2);
    }

    public function getFinalResult(): array
    {
        return $this->getResultGames(2);
    }

    private function getResultGames(int $count): array
    {
        $result = [];
        for ($i = 0; $i < $count; $i++) {
            $number1 = $this->faker->numberBetween(0, 7);
            $number2 = $this->faker->numberBetween(0, 7);

            $result[] = [
                $number1,
                ($number1 !== $number2) ? $number2 : $number2 + 1,
            ];
        }

        return $result;
    }
}
