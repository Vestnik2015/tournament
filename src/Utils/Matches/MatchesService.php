<?php

namespace App\Utils\Matches;

use App\Entity\Matches;
use App\Repository\MatchesRepository;
use Doctrine\ORM\EntityManagerInterface;

class MatchesService
{
    private EntityManagerInterface $entityManager;

    private MatchesRepository $matchesRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        MatchesRepository $matchesRepository
    ) {
        $this->entityManager = $entityManager;
        $this->matchesRepository = $matchesRepository;
    }

    public function setTotal(array $divisions): void
    {
        $this->matchesRepository->deleteAll();

        foreach ($divisions['division1']['score'] as $item) {
            $total = (new Matches())
                ->setTeam($item['team'])
                ->setTotal($item['total'])
                ->setDivision(1)
                ->setStage('division');

            $this->entityManager->persist($total);
        }

        foreach ($divisions['division2']['score'] as $item) {
            $total = (new Matches())
                ->setTeam($item['team'])
                ->setTotal($item['total'])
                ->setDivision(2)
                ->setStage('division');

            $this->entityManager->persist($total);
        }

        $this->entityManager->flush();
        $this->entityManager->clear();
    }
}
