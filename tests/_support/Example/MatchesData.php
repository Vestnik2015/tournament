<?php

namespace App\Tests\_support\Example;

class MatchesData
{
    public static $demoData = [
        'm_team' => 'team',
        'm_total' => 10,
        'm_division' => 1,
        'm_stage' => 'division',
    ];
}
