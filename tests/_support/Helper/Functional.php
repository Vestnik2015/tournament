<?php

namespace App\Tests\Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Exception;
use Throwable;

class Functional extends \Codeception\Module
{
    public function load($data)
    {
        try {
            $this->trancateTables(array_keys($data));

            foreach ($data as $table => $rows) {
                foreach ($rows as $row) {
                    try {
                        $this->getModule('Db')->haveInDatabase($table, $row);
                    } catch (Throwable $e) {
                        throw new Exception('Error when load into table ' . $table . '. data - [' . print_r(
                            $row,
                            1
                        ) . ']: ' . $e->getMessage());
                    }
                }
            }
        } catch (Throwable $e) {
            throw $e;
        }
    }

    public function trancateTables($tables)
    {
        foreach ($tables as $table) {
            $this->trancateTable($table);
        }
    }

    public function trancateTable(string $table)
    {
        $dbh = $this->getModule('Db')->_getDbh();
        $query = 'TRUNCATE TABLE %s';
        $query = sprintf($query, $table);
        $this->debugSection('Query', $query);
        $sth = $dbh->prepare($query);
        $sth->execute();
    }
}
