<?php

use Symfony\Component\Process\Process;

require dirname(__DIR__) . '/vendor/autoload.php';

if ('test' === $_ENV['APP_ENV']) {
    if ($_ENV['CLEAR_TESTS_CACHES']) {
        if ('WIN' === strtoupper(substr(PHP_OS, 0, 3))) {
            $dirName = dirname(__DIR__) . '\var\cache\test';
            $clearCacheCommand = 'if exist ' . $dirName . ' rd /s/q ' . $dirName;

            $process = Process::fromShellCommandline($clearCacheCommand);
        } else {
            $clearCacheCommand = [
                'rm',
                '-Rf',
                dirname(__DIR__) . '/var/cache/test',
            ];

            $process = new Process($clearCacheCommand);
        }

        $process->mustRun();
    }
}
