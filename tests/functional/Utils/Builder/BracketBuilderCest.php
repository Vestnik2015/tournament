<?php

namespace App\Tests\functional\Utils\Builder;

use App\Tests\_support\Example\MatchesData;
use App\Tests\FunctionalTester;
use App\Utils\Builder\BracketBuilder;

class BracketBuilderCest
{
    private BracketBuilder $service;

    public function _before(FunctionalTester $I)
    {
        $this->service = $I->grabService(BracketBuilder::class);
    }

    public function buildTest(FunctionalTester $I)
    {
        $I->load([
            'matches' => [
                array_merge(
                    MatchesData::$demoData,
                    [
                        'm_id' => 1,
                        'm_team' => 'team1',
                        'm_total' => 1,
                    ]
                ),
                array_merge(
                    MatchesData::$demoData,
                    [
                        'm_id' => 2,
                        'm_team' => 'team2',
                        'm_total' => 2,
                    ]
                ),
                array_merge(
                    MatchesData::$demoData,
                    [
                        'm_id' => 3,
                        'm_team' => 'team3',
                        'm_total' => 3,
                    ]
                ),
                array_merge(
                    MatchesData::$demoData,
                    [
                        'm_id' => 4,
                        'm_team' => 'team4',
                        'm_total' => 4,
                    ]
                ),
                array_merge(
                    MatchesData::$demoData,
                    [
                        'm_id' => 5,
                        'm_team' => 'team5',
                        'm_total' => 5,
                    ]
                ),
                array_merge(
                    MatchesData::$demoData,
                    [
                        'm_id' => 6,
                        'm_team' => 'team6',
                        'm_total' => 6,
                        'm_division' => 2,
                    ]
                ),
                array_merge(
                    MatchesData::$demoData,
                    [
                        'm_id' => 7,
                        'm_team' => 'team7',
                        'm_total' => 7,
                        'm_division' => 2,
                    ]
                ),
                array_merge(
                    MatchesData::$demoData,
                    [
                        'm_id' => 8,
                        'm_team' => 'team8',
                        'm_total' => 8,
                        'm_division' => 2,
                    ]
                ),
                array_merge(
                    MatchesData::$demoData,
                    [
                        'm_id' => 9,
                        'm_team' => 'team9',
                        'm_total' => 9,
                        'm_division' => 2,
                    ]
                ),
                array_merge(
                    MatchesData::$demoData,
                    [
                        'm_id' => 10,
                        'm_team' => 'team10',
                        'm_total' => 10,
                        'm_division' => 2,
                    ]
                ),
            ],
        ]);

        $result = $this->service->build();

        $I->assertSame(['team5', 'team4'], $result['teams'][0]);
        $I->assertSame(['team3', 'team2'], $result['teams'][1]);
        $I->assertSame(['team10', 'team9'], $result['teams'][2]);
        $I->assertSame(['team8', 'team7'], $result['teams'][3]);

        $I->assertSame(3, count($result['results']));
    }
}
