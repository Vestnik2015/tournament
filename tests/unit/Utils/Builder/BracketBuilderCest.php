<?php

namespace App\Tests\unit\Utils\Builder;

use App\Entity\Matches;
use App\Repository\MatchesRepository;
use App\Tests\UnitTester;
use App\Utils\Builder\BracketBuilder;
use App\Utils\Generator\DataGenerator;
use Mockery;
use Mockery\MockInterface;

class BracketBuilderCest
{
    private BracketBuilder $service;

    private DataGenerator $dataGenerator;

    private MockInterface $matchesRepositoryMock;

    public function _before(UnitTester $I)
    {
        $this->dataGenerator = $I->grabService(DataGenerator::class);
        $this->matchesRepositoryMock = Mockery::mock(MatchesRepository::class);
    }

    public function buildTest(UnitTester $I)
    {
        $criteria1 = [
            'division' => 1,
            'stage' => 'division',
        ];

        $criteria2 = [
            'division' => 2,
            'stage' => 'division',
        ];

        $orderBy = ['total' => 'DESC'];

        $quarterFinal1 = [
            (new Matches())->setTeam('team1'),
            (new Matches())->setTeam('team2'),
            (new Matches())->setTeam('team3'),
            (new Matches())->setTeam('team4'),
        ];
        $quarterFinal2 = [
            (new Matches())->setTeam('team5'),
            (new Matches())->setTeam('team6'),
            (new Matches())->setTeam('team7'),
            (new Matches())->setTeam('team8'),
        ];

        $this->matchesRepositoryMock
            ->expects('findBy')
            ->with($criteria1, $orderBy, 4)
            ->andReturn($quarterFinal1);

        $this->matchesRepositoryMock
            ->expects('findBy')
            ->with($criteria2, $orderBy, 4)
            ->andReturn($quarterFinal2);

        $this->getService();

        $result = $this->service->build();

        $I->assertSame(['team1', 'team2'], $result['teams'][0]);
        $I->assertSame(['team3', 'team4'], $result['teams'][1]);
        $I->assertSame(['team5', 'team6'], $result['teams'][2]);
        $I->assertSame(['team7', 'team8'], $result['teams'][3]);

        $I->assertSame(3, count($result['results']));
    }

    private function getService()
    {
        $this->service = new BracketBuilder(
            $this->dataGenerator,
            $this->matchesRepositoryMock
        );
    }
}
